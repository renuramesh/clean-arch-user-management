package migrations

import (
	"clean-code/config"
	"clean-code/internal/consts"
	"clean-code/internal/repo/driver"

	_ "github.com/golang-migrate/migrate/database/postgres"
	"github.com/golang-migrate/migrate/v4"

	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/mattes/migrate/source/file"
	"github.com/sirupsen/logrus"
)

func Migration(operation string) {
	// init the env config
	cfg, err := config.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}
	// logrus init
	log := logrus.New()

	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database")
		return
	}
	// Resolve the migration file path within the container
	// migrationsPath := filepath.Join("migrations", "scripts")

	// // Get the absolute path to the migration files
	// wd, err := os.Getwd()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// absolutePath := filepath.Join(wd, migrationsPath)
	driverDB, err := postgres.WithInstance(pgsqlDB, &postgres.Config{})
	if err != nil {
		log.Fatal(err)
	}
	// migration instance creation
	// log.Printf("Absolute path to migration files: %s", absolutePath)
	m, err := migrate.NewWithDatabaseInstance(
		"file://./migrations/scripts",
		consts.DatabaseType, driverDB)
	if err != nil {
		panic(err)
	}
	if operation == "up" {
		err = m.Up()
	}
	if operation == "down" {
		err = m.Down()
	}
	if err != nil && err != migrate.ErrNoChange {
		panic(err)
	}

	log.Printf("migration %v completed...", operation)

}
