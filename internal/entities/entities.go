package entities

import (
	"time"

	"github.com/google/uuid"
)

// User represents the user information
type User struct {
	ID         uuid.UUID
	Email      string    `json:"email"`
	Password   string    `json:"password"`
	Name       string    `json:"name"`
	Gender     string    `json:"gender"`
	DOB        string    `json:"dob"`
	CreatedAt  time.Time `json:"created_at"`
	Is_Active  bool      `json:"is_active"`
	Is_Deleted bool      `json:"is_deleted"`
}
type ErrorResponse struct {
	StatusCode int    `json:"status_code"`
	Error      error  `json:"error,omitempty"`
	Message    string `json:"message"`
}
type MetaData struct {
	Total       int `json:"total,omitempty"`
	PerPage     int `json:"per_page,omitempty"`
	CurrentPage int `json:"current_page,omitempty"`
	Next        int `json:"next,omitempty"`
	Prev        int `json:"prev,omitempty"`
}
type Response struct {
	StatusCode int         `json:"status_code"`
	Message    string      `json:"message"`
	MetaData   MetaData    `json:"meta_data,omitempty"`
	Data       interface{} `json:"data,omitempty"`
}
