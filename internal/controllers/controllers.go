package controllers

import (
	"clean-code/internal/entities"
	"clean-code/internal/usecases"
	"clean-code/version"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// UserController struct
type UserController struct {
	router   *gin.RouterGroup
	useCases usecases.UserUseCaseImply
}

// NewUserControllers creates controller struct
func NewUserControllers(router *gin.RouterGroup, appUseCase usecases.UserUseCaseImply) *UserController {
	return &UserController{
		router:   router,
		useCases: appUseCase,
	}
}

// InitRoutes intialise the routes
func (app *UserController) InitRoutes() {

	// Health handler
	app.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, app, "HealthHandler")
	})

	// Delete a user
	app.router.DELETE("/:version/users/:user_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, app, "DeleteUser")
	})
	// Update user details
	app.router.PATCH("/:version/users/:user_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, app, "UpdateUser")
	})
}

// HealthHandler check api health
func (app *UserController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

// DeleteUser for delete a user
func (app *UserController) DeleteUser(ctx *gin.Context) {
	// Parse pagination parameters from query string

	var userId string
	paramUserId := ctx.Param("user_id")
	if paramUserId == "" {
		ctx.JSON(http.StatusBadRequest, entities.ErrorResponse{
			StatusCode: 400,
			Error:      fmt.Errorf("user_id parameter is empty"),
			Message:    "user_id parameter is empty",
		})
		return
	}
	userId = paramUserId

	err := app.useCases.DeleteUser(ctx, userId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, entities.ErrorResponse{
			StatusCode: 500,
			Error:      fmt.Errorf("failed to delete user"),
			Message:    err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, entities.Response{
		StatusCode: 200,
		Message:    "user deleted successfully",
	})
}

// UpdateUser function is to update details of user
func (app *UserController) UpdateUser(ctx *gin.Context) {
	userId := ctx.Param("user_id")
	var userInfo map[string]interface{}
	if userId == "" {
		ctx.JSON(http.StatusBadRequest, entities.ErrorResponse{
			StatusCode: 400,
			Error:      fmt.Errorf("Empty userid"),
			Message:    "User id is empty",
		})
		return
	}
	if err := ctx.ShouldBindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, entities.ErrorResponse{
			StatusCode: 400,
			Error:      fmt.Errorf("binding error"),
			Message:    err.Error(),
		})
		return
	}

	if err := app.useCases.UpdateUser(ctx, userId, userInfo); err != nil {
		ctx.JSON(http.StatusInternalServerError, entities.ErrorResponse{
			StatusCode: 500,
			Error:      fmt.Errorf("updation failed"),
			Message:    err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, entities.Response{
		StatusCode: 200,
		Message:    "Updated successfully",
	})
}
