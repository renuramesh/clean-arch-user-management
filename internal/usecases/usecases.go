package usecases

import (
	"clean-code/internal/repo"
	"context"
)

// UserUseCases represents the use cases for User-related operations.
type UserUseCases struct {
	repo repo.UserRepoImply
}

// UserUseCaseImply is an interface for the User use case implementation.
type UserUseCaseImply interface {
	DeleteUser(context.Context, string) error
	UpdateUser(context.Context, string, map[string]interface{}) error
}

// NewUserUseCases creates a new instance of UserUseCases.
func NewUserUsecases(UserRepo repo.UserRepoImply) UserUseCaseImply {
	return &UserUseCases{
		repo: UserRepo,
	}
}

// DeleteUser deletes a user by its ID using the provided context.
func (dis *UserUseCases) DeleteUser(ctx context.Context, userId string) error {
	return dis.repo.DeleteUser(ctx, userId)
}
func (use *UserUseCases) UpdateUser(ctx context.Context, userId string, user map[string]interface{}) error {
	return use.repo.UpdateUser(ctx, userId, user)
}
