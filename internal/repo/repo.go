package repo

import (
	"clean-code/internal/entities"
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
)

// uSERserRepo struct
type UserRepo struct {
	db  *sql.DB
	env *entities.EnvConfig
}

// UserRepoImply is an interface for the User repository implementation.
type UserRepoImply interface {
	DeleteUser(context.Context, string) error
	UpdateUser(context.Context, string, map[string]interface{}) error
}

// NewUserserRepo creates a new instance of UserRepo.
func NewUserRepo(db *sql.DB, env *entities.EnvConfig) UserRepoImply {
	return &UserRepo{db: db, env: env}
}

// DeleteUser soft deletes a user by its ID from the database if it is not currently in use.
func (dis *UserRepo) DeleteUser(ctx context.Context, userId string) error {
	log := logrus.New()
	// Check user already inactive
	var isActive bool
	query := fmt.Sprintf(`SELECT EXISTS(SELECT 1 FROM users WHERE id = $1 AND is_active IN ('%s'))`, "true")

	err := dis.db.QueryRow(query, userId).Scan(&isActive)
	if err != nil {
		log.Errorf("failed to fetch data : %v", err)
		return err
	}
	if !isActive {
		log.Errorf("user already inactive : %v", err)
		return fmt.Errorf("user already inactive")
	}

	query = `
        UPDATE users
        SET is_deleted = $1, is_active = false
        WHERE id = $2
    `
	_, err = dis.db.Exec(query, true, userId)
	if err != nil {
		log.Errorf("error deleting app data %v", err)
		return err
	}
	return nil
}
func (use *UserRepo) UpdateUser(ctx context.Context, userId string, userData map[string]interface{}) error {
	log := logrus.New()

	var updates []string
	var values []interface{}
	counter := 1

	for field, value := range userData {
		updates = append(updates, fmt.Sprintf("%s=$%d", field, counter))
		values = append(values, value)
		counter++
	}

	query := fmt.Sprintf(`
		UPDATE users 
		SET %s
		WHERE id=$%d`, strings.Join(updates, ","), counter)

	values = append(values, userId)

	_, err := use.db.Exec(query, values...)
	if err != nil {
		log.Errorf("Error updating user data: %v", err)
		return err
	}

	return nil
}
